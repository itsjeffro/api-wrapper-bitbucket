##Introduction
This api wrapper uses the endpoints from v2 of the Bitbucket API.

https://developer.atlassian.com/bitbucket/api/2/reference/

##Install
```
$ composer install
```

##Include alias
To be able to use the main client to access the endpoints, first include an alias to the client.

```
<?php

use Itsjeffro\Bitbucket\Client;

$bitbucket = new Client;
```

##Endpoint examples

####Repositories
```php
<?php

$data = $bitbucket->repositories('owner')->get();
```

#####Available methods
You can chain the following methods to the main resource method. More available methods
for each endpoint resource can be viewed in the Resources directory.

```
$bitbucket
    ->repositories('owner', 'repo')
    ->branches()
    ->get();
```

```
/**
 * @param string $name (optional) Return branch data if a name is specified
 * @return self 
 */
->branches()

/**
 * @param string $revision (optional)
 * @return self
 */
->commits()
```

####Snippets
```
<?php

$data = $bitbucket->snippets('owner')->get();
```

####Teams
```
<?php

$data = $bitbucket->teams()->get();
```

####Users
```
<?php

$data = $bitbucket->users('username')->get();
```

## Additional methods
There are also some conventient methods for the resource endpoints.

####Partial responses
#####Fields
```
<?php

$data = $bitbucket
    ->repositories('owner', 'repo')
    ->fields([
        'next',
        'pagelen',
        'values.name', 
        'values.full_name' 
    ])
    ->get();
```

## Pagination
The first param is the number (10) of results per page. The second is the page (1) to be on.

```
<?php

$data = $bitbucket
    ->repositories('owner', 'repo')
    ->paginate(10, 1);
```