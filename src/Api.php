<?php 

namespace Itsjeffro\Bitbucket;

use Itsjeffro\Bitbucket\Http\Client;
use Exception;

class Api
{
    const API_PREFIX = 'https://api.bitbucket.org';
    
    const API_VERSION = '2.0';
    
    /**
     * @var string 
     */
    private $endpoint;
    
    /**
     * @var string 
     */
    private $resource;
    
    /**
     * @var array 
     */
    private $endpointParams = [];
    
    /**
     * Get api base uri.
     *
     * @return string
     */ 
    public function getApiPrefix()
    { 
        return self::API_PREFIX;
    }
    
    /**
     * Get api version.
     *
     * @return string
     */ 
    public function getApiVersion()
    { 
        return self::API_VERSION;
    }
    
    /**
     * Get api url.
     *
     * @return string
     */ 
    public function getApiUrl()
    { 
        return $this->getApiPrefix() . '/' . $this->getApiVersion();
    }
    
    /**
     * Set endpoint.
     *
     * @param string
     */
    public function setEndpoint($endpoint)
    {
        $resource = ($this->resource) ? $this->resource . '/' : '';
        
        if (!$this->endpoint) {
            $this->resource = $endpoint;
        }
        
        $this->endpoint = $resource . $endpoint;
    }
    
    /**
     * Set endpoint.
     *
     * @param string
     */
    public function getEndpoint()
    {        
        return $this->endpoint;
    }
    
    /**
     * Store fields to return in request.
     *
     * @param array $fields
     * @return self
     */
    public function fields($fields = [])
    {
        $this->endpointParams['fields'] = (is_array($fields) && !empty($fields)) ? $fields : [];
        
        return $this;
    }
    
    /**
     * Build the params for the endpoint to return. Includes fields, query, sort etc.
     * 
     * @return array
     */
    public function buildQuery()
    {
        $query = [];
        
        foreach ($this->endpointParams as $key => $value) {
            if (!empty($value)) {
                $query[$key] = implode(',', $value);
            }
        }
        
        return $query;
    }
    
    /**
     * Get full api url.
     *
     * @return string
     */ 
    public function getUrl()
    {
        if (empty($this->endpoint)) {
            throw new Exception('No endpoint specified');
        }
        
        return $this->getApiUrl() . '/' . $this->endpoint;
    }

    /**
     * Return list from response.
     *
     * @return array
     */
    public function get()
    {
        $response = $this->request('GET', $this->getUrl(), [
                'query' => $this->buildQuery(),
            ]);
        
        return json_decode($response->getBody());
    }

    /**
     * Return first record from response.
     *
     * @return array
     */
    public function first()
    {       
        $this->endpointParams['pagelen'] = [1];
       
        return $this->get();
    }

    /**
     * Return first record from response.
     *
     * @param integer $limit
     * @param integer $page
     * @return array
     */
    public function paginate($limit = 10, $page = 1)
    {       
        $this->endpointParams['pagelen'] = [$limit];
        
        $this->endpointParams['page'] = [$page];       
       
        return $this->get();
    }

    /**
     * Return http client object.
     *
     * @return object
     */
    public function getClient()
    {
        return $this->request('GET', $this->getUrl(), [
            'query' => $this->buildQuery(),
        ]);
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array  $options
     */
    public function request($method, $endpoint, $options = []) 
    {
        $options['headers'] = [
            'Accept' => 'application/json',
        ];

        $http = new Client;
        
        return $http->request($method, $endpoint, $options);
    }
}