<?php

namespace Itsjeffro\Bitbucket\Http;

use Exception;

class Client
{      
    /**
     * Http client contructor
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Process curl request.
     *
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return array
     */ 
    public function request($method, $endpoint, array $options = [])
    {
        $ch = curl_init();
        
        $queries = $this->parseHttpQuery($options);
        
        curl_setopt($ch, CURLOPT_URL, $endpoint . $queries);            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $headers = $this->applyHeaders($options);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        
        if (strtolower($method) === 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($options['form_params']));
        }
        
        $body = curl_exec($ch);
        
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch) . '" - Code: ' . curl_errno($ch)); 
        }
        
        $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);
        
        return new RequestHandler($httpStatusCode, $body);
    }
    
    /**
     * Parse array to http query string.
     *
     * @param array $options
     * @return string
     */
    private function parseHttpQuery(array $options)
    {
        return (is_array($options) && array_key_exists('query', $options)) ? '?' . http_build_query($options['query']) : '';
    }
    
    /**
     * Apply array to http headers. Set defaults and then override if there are any.
     *
     * @param array $options
     * @return string
     */
    private function applyHeaders(array $options)
    {
        $headers = [];
        
        if (!isset($options['headers']) || !array_key_exists('User-Agent', $options['headers'])) {
            $options['headers']['User-Agent'] = $this->defaultUserAgent();
        }
        
        foreach ($options['headers'] as $name => $value) {
            $headers[] = $name . ': ' . $value;
        }
        
        return $headers;
    }
    
    /**
     * Default user agent. Borrowed from guzzle.
     *
     * @return string
     */
    private function defaultUserAgent()
    {
        static $defaultAgent = '';

        if (!$defaultAgent) {
            $defaultAgent = 'WebRepo/1.0';
            
            if (extension_loaded('curl') && function_exists('curl_version')) {
                $defaultAgent .= ' curl/' . \curl_version()['version'];
            }
            
            $defaultAgent .= ' PHP/' . PHP_VERSION;
        }
        
        return $defaultAgent;
    }
}