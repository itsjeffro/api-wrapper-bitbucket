<?php

namespace Itsjeffro\Bitbucket\Http;

class RequestHandler
{
    private $body;
    
    private $httpStatusCode;
    
    /**
     * [construct]
     */
    public function __construct($status, $body)
    {
        $this->body = $body;
        
        $this->httpStatusCode = $status;
    }
    
    public function getBody()
    {
        return $this->body;
    }
    
    public function getStatusCode()
    {
        return $this->httpStatusCode;
    }
}