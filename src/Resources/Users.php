<?php

namespace Itsjeffro\Bitbucket\Resources;

use Itsjeffro\Bitbucket\Api;

class Users extends Api
{
    const ENDPOINT = 'users';

    /**
     * [construct]
     */
    public function __construct($username)
    {        
        $this->setEndpoint(self::ENDPOINT . '/' . $username);
    }
    
    /**
     * Get the user's followers.
     *
     * @return self
     */
    public function followers()
    {
        $this->setEndpoint('followers');        
        
        return $this;
    }
    
    /**
     * Get the users this user is following.
     *
     * @return self
     */
    public function following()
    {
        $this->setEndpoint('following');        
        
        return $this;
    }
    
    /**
     * Get the user's hooks.
     *
     * @return self
     */
    public function hooks($uid = null)
    {
        $uid = ($uid) ? '/' . $uid : '';
        
        $this->setEndpoint('hooks' . $uid);    
        
        return $this;
    }
    
    /**
     * Get the user's piplines config.
     *
     * @return self
     */
    public function pipelines_config($variableUid = null)
    {
        $variableUid = ($variableUid) ? '/' . $variableUid : '';
        
        $this->setEndpoint('pipelines_config' . $variableUid);
        
        return $this;
    }
    
    /**
     * All repositories owned by a user/team. This includes private repositories, 
     * but filtered down to the ones that the calling user has access to.
     *
     * @return self
     */
    public function repositories()
    {
        $this->setEndpoint('repositories');
        
        return $this;
    }
}