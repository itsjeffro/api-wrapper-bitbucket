<?php

namespace Itsjeffro\Bitbucket\Resources;

use Itsjeffro\Bitbucket\Api;

class Teams extends Api
{
    const ENDPOINT = 'teams';

    public function __construct()
    {        
        $this->setEndpoint(self::ENDPOINT . $owner);
    }
}