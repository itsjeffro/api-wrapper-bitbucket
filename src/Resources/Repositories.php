<?php

namespace Itsjeffro\Bitbucket\Resources;

use Itsjeffro\Bitbucket\Api;
use Exception;

class Repositories extends Api
{
    const ENDPOINT = 'repositories';
    
    /**
     * @var string
     */
    public $owner;
    
    /**
     * @var string
     */
    public $repo;

    /**
     * [construct]
     */
    public function __construct($owner = null, $repo = null)
    {  
        $this->owner = ($owner) ? '/' . $owner : '';
        
        $this->repo = ($repo) ? '/' . $repo : '';
        
        $this->setEndpoint(self::ENDPOINT . $this->owner . $this->repo);
    }
    
    /**
     * Get branches belonging to a repository.
     *
     * @param string $name
     * @return self
     */
    public function branches($name = null)
    {
        if (empty($this->repo)) {
            throw new Exception('A repository must be specified');
        }
        
        $name = $name ? '/'.$name : null;
        
        $this->setEndpoint('refs/branches' . $name);
               
        return $this;
    }
    
    /**
     * Get commits belonging to a repository.
     *
     * @param string $revision
     * @return self
     */
    public function commits($revision = null)
    {
        if (empty($this->repo)) {
            throw new Exception('A repository must be specified');
        }
        
        $revision = $revision ? '/'.$revision : null;
        
        $this->setEndpoint('commits' . $revision);
               
        return $this;
    }
    
    /**
     * Get commit belonging to a repository.
     *
     * @return self
     */
    public function commit()
    {
        if (empty($this->repo)) {
            throw new Exception('A repository must be specified');
        }
        
        $this->setEndpoint('commit');        
        
        return $this;
    }
    
    /**
     * Returns the components that have been defined in the issue tracker. 
     * This resource is only available on repositories that have the 
     * issue tracker enabled.
     *
     * @return self
     */
    public function components()
    {
        if (empty($this->repo)) {
            throw new Exception('A repository must be specified');
        }
        
        $this->setEndpoint('components');        
        
        return $this;
    }
    
    /**
     * Returns the repository's default reviewers. These are the users 
     * that are automatically added as reviewers on every new pull 
     * request that is created.
     *
     * @return self
     */
    public function defaultReviewers()
    {
        if (empty($this->repo)) {
            throw new Exception('A repository must be specified');
        }
        
        $this->setEndpoint('default-reviewers');        
        
        return $this;
    }
    
    /**
     * Produces a raw, git-style diff for either a single commit (diffed against its first
     * parent), or a revspec of 2 commits (e.g.3a8b42..9ff173 where the first 
     * commit represents the source and the second commit the destination).
     *
     * @return self
     */
    public function diff($spec)
    {
        if (empty($this->repo)) {
            throw new Exception('A repository must be specified');
        }
        
        $this->setEndpoint('diff/' . $spec);        
        
        return $this;
    }
}