<?php

namespace Itsjeffro\Bitbucket\Resources;

use Itsjeffro\Bitbucket\Api;

class Snippets extends Api
{
    const ENDPOINT = 'snippets';

    public function __construct($owner = null, $encoded_id = null)
    {   
        $owner = ($owner) ? '/' . $owner : '';
        
        $encoded_id = ($encoded_id) ? '/' . $encoded_id : '';
  
        $this->setEndpoint(self::ENDPOINT . $owner . $encoded_id);
    }
    
    /**
     * Get comments belonging to snippet.
     *
     * @return self
     */
    public function comments()
    {
        $this->setEndpoint('comments');
               
        return $this;
    }
    
    /**
     * Get commits belonging to snippet.
     *
     * @return self
     */
    public function commits()
    {
        $this->setEndpoint('commits');
               
        return $this;
    }
    
    /**
     * Used to check if the current user is watching a specific snippet.
     *
     * @return self
     */
    public function watch()
    {
        $this->setEndpoint('watch');
               
        return $this;
    }
    
    /**
     * Returns a paginated list of all users watching a specific snippet..
     *
     * @return self
     */
    public function watchers()
    {
        $this->setEndpoint('watchers');
               
        return $this;
    }
}