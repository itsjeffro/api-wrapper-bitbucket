<?php

namespace Itsjeffro\Bitbucket;

use Itsjeffro\Bitbucket\Resources\Snippets;
use Itsjeffro\Bitbucket\Resources\Repositories;
use Itsjeffro\Bitbucket\Resources\Teams;
use Itsjeffro\Bitbucket\Resources\Users;

class Client {      
    /**
     * @param string $owner
     * @param string $repo
     * @return object
     */
    public function repositories($owner = null, $repo = null)
    {        
        $resource = new Repositories($owner, $repo);       
        return $resource;
    }
    
    /**
     * @param string $owner
     * @return object
     */
    public function snippets($owner = null)
    {
        $resource = new Snippets($owner);       
        return $resource;
    }
    
    /**
     * @return object
     */
    public function teams()
    {
        $resource = new Teams();        
        return $resource;
    }   
    
    /**
     * @param string $user
     * @return object
     */
    public function users($username)
    {
        $resource = new Users($username);       
        return $resource;
    } 
}